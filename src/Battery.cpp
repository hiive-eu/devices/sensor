#include "Battery.h"

namespace battery {
  void enable() {
    pinMode(PIN_ADC_ANALOGSWITCH, INPUT);
    pinMode(PIN_DOUT_TOGGLE_ANALOGSWITCH, OUTPUT);
    digitalWrite(PIN_DOUT_TOGGLE_ANALOGSWITCH, 0x00);
    delay(200);
    for (int i=0; i<BATTERY_WARMUP_SAMPLES; i++) {
      int reading = analogRead(PIN_ADC_BATTERY);
    }
  }
  
  void disable() { // ALSO DISABLES MICROPHONE
    pinMode(PIN_DOUT_TOGGLE_ANALOGSWITCH, INPUT);
  }
  
  uint8_t readPercentage() {
    uint16_t voltage = readVoltage();
    return batteryVoltageToPercentage(voltage);
  }
  
  uint16_t readVoltage() {
    uint32_t bat_reads = 0;
    for (int i = 0; i<BATTERY_SAMPLES; i++) {
      int reading = analogRead(PIN_ADC_BATTERY);
      float _reading = (float)(reading * 2400)/4095;
      uint16_t voltage = (_reading*(VBAT_R55+VBAT_R54))/VBAT_R55;
      bat_reads += voltage;
    }
    return (int)((float)bat_reads / BATTERY_SAMPLES);
  }
  
  uint8_t batteryVoltageToPercentage(uint16_t voltage) {
    if (voltage < VBAT_MIN) {voltage = VBAT_MIN;};
    if (voltage > VBAT_MAX) {voltage = VBAT_MAX;};
    return map(voltage, VBAT_MIN, VBAT_MAX, 0, 100);
  }
}
