#include "Microphone.h"

namespace microphone {
  uint32_t startTime = 0;
  uint32_t stopTime = 0;
  uint32_t sampleTime = 0;
  uint32_t sampleRateDelay = 0;
  float sampleRate = 0;
  float averageSampleRate = 0;
  float averageSoundLevel = 0;
  float averageFftResolution = 0;
  float stdVal = 0;
  float dbSPL = 0;
  
  int16_t data[NUM_OF_SAMPLES] = {0};
  int16_t im[NUM_OF_SAMPLES] = {0};
  float fftRes[NUM_OF_SAMPLES/4] = {0.0}; // This can be removed to save RAM if reusing the "data" or "im" array to store the result.
  
  void enable() {
    pinMode(PIN_ADC_ANALOGSWITCH, INPUT);
    pinMode(PIN_DOUT_TOGGLE_ANALOGSWITCH, OUTPUT);
    digitalWrite(PIN_DOUT_TOGGLE_ANALOGSWITCH, 1);
    delay(200);
  }
  
  void disable() { // ALSO DISABLES BATTERY
    pinMode(PIN_DOUT_TOGGLE_ANALOGSWITCH, INPUT);
  }
  
  float record(float bins[]) {
    //reset values
    startTime = 0;
    stopTime = 0;
    sampleTime = 0;
    sampleRateDelay = 0;
    sampleRate = 0;
    averageSampleRate = 0;
    averageSoundLevel = 0;
    averageFftResolution = 0;
    stdVal = 0;
    dbSPL = 0;
    for (int i=0; i<NUM_OF_FREQUENCY_BINS; i++) {
      bins[i] = 0.0;
    }
    for (int i=0; i<NUM_OF_SAMPLES/4; i++) {
      fftRes[i] = 0.0;
    }
    for (int i=0; i<NUM_OF_SAMPLES; i++) {
      data[i] = 0;
      im[i] = 0;
    }
  
    sampleRateDelay = GetSampleRateDelay(SAMPLE_RATE);
    for (int i=0; i<NUM_OF_RECORDINGS; i++) {
      startTime = micros();
  
      for(int j=0; j<NUM_OF_SAMPLES; j++) {
        data[j] = (uint16_t)analogRead(PIN_ADC_MICROPHONE);
        delayMicroseconds(sampleRateDelay);
      }
      
      stopTime = micros();
      stdVal = sd(data, NUM_OF_SAMPLES);
      dbSPL = soundPressureLevel(stdVal);
  
     fix_fft(data, im, 10, 0);
  
      for(int j=0; j<NUM_OF_SAMPLES/4; j++)
      {
        if(j==0)
        {
          // Første FFT bin repræsentere DC niveauet og skal ikke ganges med 2.
          fftRes[j] += (sqrt(data[j] * data[j] + im[j] * im[j])) / NUM_OF_RECORDINGS;
        }
        else
        {
          // Vi ganger med 2 da energien for hver frekvens bin er repræsenteret ligeligt i den positive og negative del af spektrummet (Vi benytter kun den positive del).
          fftRes[j] += 2 * (sqrt(data[j] * data[j] + im[j] * im[j])) / NUM_OF_RECORDINGS;
        }
      }
      sampleTime = stopTime - startTime;
      sampleRate = (float)NUM_OF_SAMPLES / ((float)sampleTime / US_TO_SEC);
      averageSampleRate += sampleRate / NUM_OF_RECORDINGS;
      averageSoundLevel += dbSPL / NUM_OF_RECORDINGS;
    }
    averageFftResolution = averageSampleRate/NUM_OF_SAMPLES;
    for(int i = 1; i<NUM_OF_SAMPLES/4; i++)
    {
      float freq = averageFftResolution * i;
      uint8_t bin_idx = freq/BIN_FREQUENCY_STEP;
      if (bin_idx < NUM_OF_FREQUENCY_BINS) {
        if (fftRes[i] > bins[bin_idx]) {
          bins[bin_idx] = fftRes[i];
        }
      } else {
        if (fftRes[i] > bins[NUM_OF_FREQUENCY_BINS-1]) {
          bins[NUM_OF_FREQUENCY_BINS-1] = fftRes[i];
        }
      }
    }
    for (int i=0; i<NUM_OF_FREQUENCY_BINS; i++) {
      bins[i] = soundPressureLevel(bins[i]);
    }
    return averageSoundLevel;
  }
  
  /*
    brief Get sample time of collection NUM_OF_SAMPLES when no delay is used. This varies both based on boards but also temperature.
    Written by: Christoffer Gjedsted Brask <cgb@develco.dk>
  */
  float GetSampleTimeNoDelay(void)
  {
    startTime = micros();
    for(int i = 0; i<NUM_OF_SAMPLES; i++)
    {
      data[i] = (int16_t)analogRead(PIN_ADC_MICROPHONE);
    }
    stopTime = micros();
    sampleTime = stopTime - startTime;
    return sampleTime/(float)NUM_OF_SAMPLES;
  }
  
  /*
    Approximates the delay needed between each ADC measurement to achieve the target sample rate
    sampleRate The target sample rate cannot be higher than ~ 9385 Hz.
    Written by: Christoffer Gjedsted Brask <cgb@develco.dk>
  */
  uint32_t GetSampleRateDelay(uint16_t sampleRate)
  {
    float noDelaySampleTime = GetSampleTimeNoDelay();  
    float targetSampleTime = 1.0 / sampleRate * US_TO_SEC;
    float timeSpentCallingDelayFunc = 1.11812 + 0.00312 * (targetSampleTime-noDelaySampleTime);
    return round((targetSampleTime-noDelaySampleTime-timeSpentCallingDelayFunc));
  }
  
  /**
   Calculate the sound pressure level from a raw ADC value
   The mic output is amplified by 101 (1+(270/2.7)) from the opamp. 
   The mic sensitivity is -40dBV to -36dBV which means the mic output is 10^(-40/20)=0.01.
   Based on this the voltage is not divided by 101 from the opamp gain as the output needs
   to be multiplied by 100 when include the sensitivity which is more or less the same value.
   Written by: Christoffer Gjedsted Brask <cgb@develco.dk>
  */
  float soundPressureLevel(float rawAdcVal)
  {
    /**
    R24 1800
    R26 750
    amp: 1+(270/0.75) = 361
    adjust: 79.43 / 361 = 0.22
    */
    float voltage = VADC_REF / ADC_RES * rawAdcVal * 0.22;
    float dbSPL = 20.0f * log10f(voltage) + MIC_REF_SPL;
  
    return dbSPL;
  }
  /**
   Calculate the standard deviation
   Returns the deviation of the collected samples
   Written by: Christoffer Gjedsted Brask <cgb@develco.dk>
  */
  float sd(int16_t *data, uint16_t len)
  {
    float std = 0;
    float mean = 0;
    uint32_t sum = 0;
    float sumSquare = 0;
  
    for (uint16_t i = 0; i < len; i++)
    {
      sum += data[i];
    }
  
    mean = sum / (float)len;
  
    for (uint16_t i = 0; i < len; i++)
    {
      sumSquare += pow(data[i] - mean, 2);
    }
  
    std = sqrtf(sumSquare / len);
    
    return std;
  }
  
  
  /*
    FIX_M,PY() - ,fixed-p,oint multiplication & scaling.
    Substitute inline assembly for hardware-specific
    optimization suited to a particluar DSP processor.
    Scaling ensures that result remains 16-bit.
    Written by:  Tom Roberts  11/8/89
    Made portable:  Malcolm Slaney 12/15/94 malcolm@interval.com
    Enhanced:  Dimitrios P. Bouras  14 Jun 2006 dbouras@ieee.org
  */
  inline int16_t FIX_MPY(int16_t a, int16_t b)
  {
    /* shift right one less bit (i.e. 15-1) */
    int32_t c = ((int32_t)a * (int32_t)b) >> 14;
    /* last bit shifted out = rounding-bit */
    b = c & 0x01;
    /* last shift + rounding bit */
    a = (c >> 1) + b;
    return a;
  }
  
  /*
    fix_fft() - perform forward/inverse fast Fourier transform.
    fr[n],fi[n] are real and imaginary arrays, both INPUT AND
    RESULT (in-place FFT), with 0 <= n < 2**m; set inverse to
    0 for forward transform (FFT), or 1 for iFFT.
    Written by:  Tom Roberts  11/8/89
    Made portable:  Malcolm Slaney 12/15/94 malcolm@interval.com
    Enhanced:  Dimitrios P. Bouras  14 Jun 2006 dbouras@ieee.org
  */
  int32_t fix_fft(int16_t fr[], int16_t fi[], int16_t m, int16_t inverse)
  {
    int32_t mr, nn, i, j, l, k, istep, n, scale, shift;
    int16_t qr, qi, tr, ti, wr, wi;
    n = 1 << m;
    /* max FFT size = N_WAVE */
    if (n > N_WAVE)
      return -1;
    mr = 0;
    nn = n - 1;
    scale = 0;
    /* decimation in time - re-order data */
    for (m=1; m<=nn; ++m) {
      l = n;
      do {
        l >>= 1;
      } while (mr+l > nn);
      mr = (mr & (l-1)) + l;
      if (mr <= m)
        continue;
      tr = fr[m];
      fr[m] = fr[mr];
      fr[mr] = tr;
      ti = fi[m];
      fi[m] = fi[mr];
      fi[mr] = ti;
    }
    l = 1;
    k = LOG2_N_WAVE-1;
    while (l < n) {
      if (inverse) {
        /* variable scaling, depending upon data */
        shift = 0;
        for (i=0; i<n; ++i) {
          j = fr[i];
          if (j < 0)
            j = -j;
          m = fi[i];
          if (m < 0)
            m = -m;
          if (j > 16383 || m > 16383) {
            shift = 1;
            break;
          }
        }
        if (shift)
          ++scale;
      } else {
        /*
          fixed scaling, for proper normalization --
          there will be log2(n) passes, so this results
          in an overall factor of 1/n, distributed to
          maximize arithmetic accuracy.
        */
        shift = 1;
      }
      /*
        it may not be obvious, but the shift will be
        performed on each data point exactly once,
        during this pass.
      */
      istep = l << 1;
      for (m=0; m<l; ++m) {
        j = m << k;
        /* 0 <= j < N_WAVE/2 */
        wr =  Sinewave[j+N_WAVE/4];
        wi = -Sinewave[j];
        if (inverse)
          wi = -wi;
        if (shift) {
          wr >>= 1;
          wi >>= 1;
        }
        for (i=m; i<n; i+=istep) {
          j = i + l;
          tr = FIX_MPY(wr,fr[j]) - FIX_MPY(wi,fi[j]);
          ti = FIX_MPY(wr,fi[j]) + FIX_MPY(wi,fr[j]);
          qr = fr[i];
          qi = fi[i];
          if (shift) {
            qr >>= 1;
            qi >>= 1;
          }
          fr[j] = qr - tr;
          fi[j] = qi - ti;
          fr[i] = qr + tr;
          fi[i] = qi + ti;
        }
      }
      --k;
      l = istep;
    }
    return scale;
  }
}
