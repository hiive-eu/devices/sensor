#include "Misc.h"

void copyMacBytesToAscii(char macAscii[13], uint8_t macBytes[6]) {
  for (int i = 0; i< 6; i++) {
    sprintf(macAscii + i * 2, "%02X", macBytes[i]);
  }
  macAscii[12] = 0x00;
}

bool compareMacs(uint8_t mac1[6], uint8_t mac2[6]) {
  for (size_t i = 0; i < 6; i++) {
    if (mac1[i] != mac2[i]) {
      return false;
    }
  }
  return true;
}
