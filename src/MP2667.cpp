#include "MP2667.h"

void MP2667::init() {
}

void MP2667::reset() {
  _writeReg(MP2667_REG_ADDR_INPUT_SRC_CTRL, 0b01001011);
  delay(100);
  _writeReg(MP2667_REG_ADDR_POWER_ON_CONF, 0b00000100);
  delay(100);
  _writeReg(MP2667_REG_ADDR_CHARGE_CURRENT_CTRL, 0b00000111);
  delay(100);
  _writeReg(MP2667_REG_ADDR_DISCHARGE_TERMINATION_CURRENT_CTRL, 0b01001001);
  delay(100);
  _writeReg(MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL, 0b10100011);
  delay(100);
  _writeReg(MP2667_REG_ADDR_CHARGE_TERMINATION_TIMER_CTRL, 0b01001010);
  delay(100);
  _writeReg(MP2667_REG_ADDR_MISC_OPS, 0b00001011);
  delay(100);
}

void MP2667::enableShippingMode() {
  uint8_t currentVal = this->_readReg(MP2667_REG_ADDR_MISC_OPS);
  uint8_t newVal = replaceBits(currentVal, 5, 5, 0b00100000);
  this->_writeReg(MP2667_REG_ADDR_MISC_OPS, newVal);
}

void MP2667::disableShippingMode() {
  pinMode(PIN_MP2667_ISR_SHIPPING, OUTPUT);
  digitalWrite(PIN_MP2667_ISR_SHIPPING, 0);
  delay(1000); // Should be above 500ms
  pinMode(PIN_MP2667_ISR_SHIPPING, INPUT);
}

// bits 0-4
uint8_t MP2667::getChargingCurrent() {
  uint8_t v =_readReg(MP2667_REG_ADDR_CHARGE_CURRENT_CTRL);
  return (MP2667ChargingCurrent)extractBits(v, 0, 4);
}

//bits 2-7
uint8_t MP2667::getChargingVoltage() {
  uint8_t v =_readReg(MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL);
  return extractBits(v, 2, 7);
}

// bits 3-4
uint8_t MP2667::getChargingStatus() {
  uint8_t v =_readReg(MP2667_REG_ADDR_SYSTEM_STATUS);
  return extractBits(v, 3, 4);
}
// bits 0-2
uint8_t MP2667::getUndervoltageLockout() {
  uint8_t v =_readReg(MP2667_REG_ADDR_POWER_ON_CONF);
  return extractBits(v, 0, 2);
}
// bits 5-5
uint8_t MP2667::getFetDis() {
  uint8_t v =_readReg(MP2667_REG_ADDR_MISC_OPS);
  return extractBits(v, 5, 5);
}

// bits 0-4
void MP2667::setChargingCurrent(MP2667ChargingCurrent v) {
  uint8_t currentVal = this->_readReg(MP2667_REG_ADDR_CHARGE_CURRENT_CTRL);
  uint8_t newVal = replaceBits(currentVal, 0, 4, v);
  this->_writeReg(MP2667_REG_ADDR_CHARGE_CURRENT_CTRL, newVal);
}

//bits 2-7
void MP2667::setChargingVoltage(MP2667ChargingVoltage v) {
  uint8_t currentVal = this->_readReg(MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL);
  uint8_t newVal = replaceBits(currentVal, 2, 7, v);
  this->_writeReg(MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL, newVal);
}

// bits 0-2
void MP2667::setUndervoltageLockout(MP2667UndervoltageLockout v) {
  uint8_t currentVal = this->_readReg(MP2667_REG_ADDR_POWER_ON_CONF);
  uint8_t newVal = replaceBits(currentVal, 0, 2, MP2667_VAL_UVLO);
  this->_writeReg(MP2667_REG_ADDR_POWER_ON_CONF, newVal);
}

void MP2667::printAllRegs() {
  Serial.println(this->_readInputSourceControlReg(), BIN);
  Serial.println(this->_readPowerOnConfReg(), BIN);
  Serial.println(this->_readChargeCurrentControlReg(), BIN);
  Serial.println(this->_readDishargeTerminationCurrentControlReg(), BIN);
  Serial.println(this->_readChargeVoltageControlReg(), BIN);
  Serial.println(this->_readChargeTerminationTimerControlReg(), BIN);
  Serial.println(this->_readMiscOperationsReg(), BIN);
  Serial.println(this->_readSystemStatusReg(), BIN);
  Serial.println(this->_readFaultReg(), BIN);
}

uint8_t MP2667::_readReg(uint8_t reg){
  Wire.beginTransmission(I2C_ADDR_MP2667);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(I2C_ADDR_MP2667, 1);
  uint8_t val = Wire.read();
  delay(100);
  return val;
}

void MP2667::_writeReg(uint8_t reg, uint8_t v){
  Wire.beginTransmission(I2C_ADDR_MP2667);
  Wire.write(reg);
  Wire.write(v);
  Wire.endTransmission();
  delay(100);
}

uint8_t MP2667::_readInputSourceControlReg() {
  return this->_readReg(MP2667_REG_ADDR_INPUT_SRC_CTRL);
}
uint8_t MP2667::_readPowerOnConfReg() {
  return this->_readReg(MP2667_REG_ADDR_POWER_ON_CONF);
}
uint8_t MP2667::_readChargeCurrentControlReg() {
  return this->_readReg(MP2667_REG_ADDR_CHARGE_CURRENT_CTRL);
}
uint8_t MP2667::_readDishargeTerminationCurrentControlReg() {
  return this->_readReg(MP2667_REG_ADDR_DISCHARGE_TERMINATION_CURRENT_CTRL);
}
uint8_t MP2667::_readChargeVoltageControlReg() {
  return this->_readReg(MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL);
}
uint8_t MP2667::_readChargeTerminationTimerControlReg() {
  return this->_readReg(MP2667_REG_ADDR_CHARGE_TERMINATION_TIMER_CTRL);
}
uint8_t MP2667::_readMiscOperationsReg() {
  return this->_readReg(MP2667_REG_ADDR_MISC_OPS);
}
uint8_t MP2667::_readSystemStatusReg() {
  return this->_readReg(MP2667_REG_ADDR_SYSTEM_STATUS);
}
uint8_t MP2667::_readFaultReg() {
  return this->_readReg(MP2667_REG_ADDR_FAULT);
}

uint8_t replaceBits(uint8_t original, int n, int m, uint8_t replacement) {
  uint8_t res = 0;
  for (int i = 7; i >= 0; i--) {
    res = res >> 1;
    if (7-i >= n && 7-i <= m) {
      uint8_t bit = (replacement << (i)) & 0b10000000;
      res = (res | bit);
    } else {
      uint8_t bit = (original << (i)) & 0b10000000;
      res = (res | bit);
    }
  }
  return res;
}

uint8_t extractBits(uint8_t value, int n, int m) {
  uint8_t length = m-n+1;
  uint8_t mask = 0b11111111 >> (8 - length);
  mask = mask << n;
  uint8_t res = (value & mask) >> n;
  return res;
}
