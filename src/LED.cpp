#include "LED.h"
#include "Hardware.h"

namespace led {
  void redOn() {
    pinMode(PIN_DOUT_LED_R, OUTPUT);
    digitalWrite(PIN_DOUT_LED_R, HIGH);
  }
  
  void redOff() {
    pinMode(PIN_DOUT_LED_R, OUTPUT);
    digitalWrite(PIN_DOUT_LED_R, LOW);
  }
  
  void greenOn() {
    pinMode(PIN_PWM_LED_G, OUTPUT);
    analogWrite(PIN_PWM_LED_G, 0xFFFF);
  }
  
  void greenOff() {
    pinMode(PIN_PWM_LED_G, OUTPUT);
    digitalWrite(PIN_PWM_LED_G, LOW);
  }
  
  void blueOn() {
    pinMode(PIN_PWM_LED_B, OUTPUT);
    analogWrite(PIN_PWM_LED_B, 0xFFFF);
  }
  
  void blueOff() {
    pinMode(PIN_PWM_LED_B, OUTPUT);
    digitalWrite(PIN_PWM_LED_B, LOW);
  }

  void cyanOn() {
  analogWrite(PIN_PWM_LED_G, 0x7FFF); // Green pin full intensity
  analogWrite(PIN_PWM_LED_B, 0x7FFF); // Blue pin full intensity
  }
  
  void magentaOn() {
    digitalWrite(PIN_DOUT_LED_R, HIGH);  // Red pin OFF
    analogWrite(PIN_PWM_LED_B, 0x7FFF); // Blue pin full intensity
  }

  void yellowOn() {
    digitalWrite(PIN_DOUT_LED_R, HIGH);  // Red pin OFF
    analogWrite(PIN_PWM_LED_G, 0x7FFF); // Green pin medium intensity
  }


  
  void blueBlink() {
    blueOn();
    delay(100);
    blueOff();
    }
  
  void allOff() {
    redOff();
    greenOff();
    blueOff();
  }
  
}
