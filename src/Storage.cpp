#include "Storage.h"

namespace storage {
  bool readInitialized() {
    EEPROM.begin(4);
    bool initialized = EEPROM.read(STORAGE_INIT_ADDR);
  #if defined(DEBUG) && DEBUG > 0
    Serial.printf("[STORAGE] Initialized <- %d\r\n", initialized);
  #endif
    EEPROM.end();
    return initialized;
  }
  
  void writeInitialized(bool initialized) {
    EEPROM.begin(4);
  #if defined(DEBUG) && DEBUG > 0
    Serial.printf("[STORAGE] Initialized -> %d\r\n", initialized);
  #endif
    EEPROM.write(STORAGE_INIT_ADDR, initialized);
    EEPROM.commit();
    EEPROM.end();
  }
}
