#include <Arduino.h>
#include <Wire.h>

#include "Adafruit_SHT4x.h"
#include "Battery.h"
#include "Defines.h"
#include "EEPROM.h"
#include "Hardware.h"
#include "LoRa.h"
#include "MC34x9.h"
#include "Microphone.h"
#include "MP2667.h"
#include "LED.h"
#include "Storage.h"

#include "Misc.h"
#include "Protocol.h"
#include "Reading.h"
#include "low_power.h"

int8_t allowedIntervals[5] = {5, 15, 20, 30, 60};
size_t allowedIntervalsCount = 5;

//uint32_t millisBeforeJump = 0;

uint32_t sendStartMillis;
uint32_t receiveStartMillis;
uint16_t currentReceiveTimeout;

constexpr uint8_t NUM_MIC_CALIBRATION_SAMPLES = 200;

constexpr uint16_t PAIR_SLEEP_S = 2;
constexpr uint16_t SENSOR_SEND_INTERVAL_MS = 1000;
constexpr uint8_t MAX_SENSORS = 10;
constexpr uint8_t MAX_PAIR_ATTEMPTS = 2;
constexpr uint16_t HEARTBEAT_INTERVAL_MS = 10000;

constexpr uint16_t M_TO_MS = 60 * 1000;
constexpr uint16_t M_TO_S = 60;
constexpr uint16_t S_TO_MS = 1000;

MP2667 mp2667;
Adafruit_SHT4x sht40;
MC34X9 mc3479;

uint64_t gMacU64 = getID();
uint8_t gMacBytes[6];
uint8_t gBattery;
uint8_t gGatewayMacBytes[6];

uint16_t gPacketNum = 0;

Reading reading;
void performReadings();
bool checkSleepDuration(int8_t interval, int8_t minutes, int8_t seconds);

void handlePairReadingResponse(uint8_t *payload);
void handleReadingResponse(uint8_t *payload);
void handleStartCycleRequest(uint8_t *payload);

uint8_t sleepIntervalMinutes;
uint8_t sensorCycleNumber = 0;

uint32_t timeWakeup = 0;
uint32_t sinceWakeupBeforeJump = 0;
//uint32_t timeWakeupAdjusted = 0;

void initialize();

static TimerEvent_t wakeUpTimer;
void startSleep(uint8_t minutesTillWakeup = 0, uint8_t secondsTillWakeup = 0, bool timeout = false);
void powerDown();
void onWakeUp();

enum ProgramState {
  LORA_S = 0x00,
  WAKEUP_S = 0x01,
  SLEEP_S = 0x02,
};

ProgramState programState;
ProgramState prevProgramState;

void setprogramState(ProgramState newProgramState) {
  prevProgramState = programState;
  programState = newProgramState;  
}

uint8_t pairAttempt = 0;
MessageCode lastSentMessageCode;
void setLastSentMessage(MessageCode lastMessageCode) {
  lastSentMessageCode = lastMessageCode;
}

void onTxDone();
void onTxTimeout();
void onRxDone(uint8_t *payload, uint16_t size, int16_t rssi);
void onRxTimeout();

void performReadings() {
  reading.battery = battery::readPercentage();

  sensors_event_t sht40_event;
  sht40.getTemperatureSensor()->getEvent(&sht40_event);
  reading.temperature = sht40_event.temperature;
  sht40.getHumiditySensor()->getEvent(&sht40_event);
  reading.humidity = sht40_event.relative_humidity;

  reading.accX = mc3479.readRawAccel().XAxis;
  reading.accY = mc3479.readRawAccel().YAxis;
  reading.accZ = mc3479.readRawAccel().YAxis;

  microphone::enable();
  float db = microphone::record(reading.frequencyBins);
  reading.soundLevel = db;

  reading.packetNum = gPacketNum;
}

void handlePairResponse(uint8_t payload[RESP_PAIR_PAYLOAD_SIZE]) {
  sensorCycleNumber = payload[7];
  gGatewayMacBytes[0] = payload[8];
  gGatewayMacBytes[1] = payload[9];
  gGatewayMacBytes[2] = payload[10];
  gGatewayMacBytes[3] = payload[11];
  gGatewayMacBytes[4] = payload[12];
  gGatewayMacBytes[5] = payload[13];
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[SYSTEM] GATEWAY MAC:  "));
  for (int i = 5; i >= 0; i--) {
    uint8_t byte = gGatewayMacBytes[i];
    if (byte < 16) {
      Serial.print(0);
    }
    Serial.print(byte, HEX);
  }
  Serial.println();
  Serial.print(F("[SYSTEM] Sensor cycle number: "));
  Serial.println(sensorCycleNumber);
  switch (sensorCycleNumber-1 % 5) {
    case 0:
      led::allOff();
      led::greenOn();
      break;
    case 1:
      led::allOff();
      led::blueOn();
      break;
    case 2:
      led::allOff();
      led::cyanOn();
      break;
    case 3:
      led::allOff();
      led::magentaOn();
      break;
    case 4:
      led::allOff();
      led::yellowOn();
      break;
    default:
      break;
  
  }
#endif
}

bool checkSleepDuration(int8_t interval, int8_t minutes, int8_t seconds) {
  bool allPositive = interval > 0 && minutes > 0 && seconds > 0;

  bool intervalAllowed = false;
  for (size_t i = 0; i < allowedIntervalsCount; i++) {
    if (interval == allowedIntervals[i]) {
      intervalAllowed = true;  // Set to true if value is found in allowedValues
      break;
    }
  }

  if (allPositive) {
    int32_t totalSeconds = minutes * M_TO_S + seconds;
    int32_t intervalSeconds = sleepIntervalMinutes * M_TO_S;
    if (!intervalAllowed) {
#if defined(DEBUG) && DEBUG > 0
      Serial.println("[SLEEPTEST] INVALID SLEEP INTERVAL");
      Serial.print("[SLEEPTEST] INTERVAL: ");
      Serial.println(interval);
#endif
      return false;
    } else if (intervalSeconds > totalSeconds) {
      return true;
    } else if (abs(interval * M_TO_S - totalSeconds) > 60 ) {
#if defined(DEBUG) && DEBUG > 0
      Serial.println("[SLEEPTEST] SLEEP DURATION HIGHER THAN INTERVAL AND SLEEP DURATION DOESNT MATCH NEW INTERVAL");
      Serial.print("[SLEEPTEST] MINUTES:  ");
      Serial.println(minutes);
      Serial.print("[SLEEPTEST] SECONDS: ");
      Serial.println(seconds);
#endif
      return false;
    } else {
      // Interval has probably been increased by the user on the gateway
      return true;
    }
  } else {
#if defined(DEBUG) && DEBUG > 0
    Serial.print("[SLEEPTEST] INTERVAL, MINUTES OR SECONDS FROM GATEWAY NEGATIVE: ");
    Serial.print("[SLEEPTEST] INTERVAL: ");
    Serial.println(interval);
    Serial.print("[SLEEPTEST] MINUTES:  ");
    Serial.println(minutes);
    Serial.print("[SLEEPTEST] SECONDS: ");
    Serial.println(seconds);
#endif
    return false;
  }
}

void handleReadingResponse(uint8_t payload[RESP_READING_PAYLOAD_SIZE]) {
  int8_t gatewayInterval = payload[7];
  int8_t gatewayMinutesSleep = payload[8];
  int8_t gatewaySecondsSleep = payload[9];
  if (checkSleepDuration(gatewayInterval, gatewayMinutesSleep, gatewaySecondsSleep)) {
    startSleep(gatewayMinutesSleep, gatewaySecondsSleep);
  } else {
    startSleep(0, 0, true);
  }
}

void handleStartCycleRequest(uint8_t payload[START_CYCLE_PAYLOAD_SIZE]) {
  sleepIntervalMinutes = payload[7];
  startSleep(payload[8], payload[9]);
}

void initialize() {
  if (!storage::readInitialized()) {
    mp2667.reset();
    mp2667.enableShippingMode();
    storage::writeInitialized(true);
    led::redOn();
    while (true) {}
  }
}

void startSleep(uint8_t minutesTillWakeup, uint8_t secondsTillWakeup, bool timeout) {
  uint32_t sleepDurationMillis = 0;
  if (timeout) {
    // TIMEOUT
    // Magic number
    sleepDurationMillis = sleepIntervalMinutes * M_TO_MS - (millis() - timeWakeup) + 500;
  } else if(minutesTillWakeup + secondsTillWakeup != 0) {
    // Cycle sleep
    uint32_t sensorCycleNumberDelay = (sensorCycleNumber-1) * SENSOR_SEND_INTERVAL_MS;

    sleepDurationMillis = minutesTillWakeup * M_TO_MS
                        + secondsTillWakeup * S_TO_MS
                        + sensorCycleNumberDelay;
  }

  if (sleepDurationMillis != 0) {
#if defined(DEBUG) && DEBUG > 0
    Serial.print(F("[SLEEP]  minutesTillWakeup: "));
    Serial.println(minutesTillWakeup);
    Serial.print(F("[SLEEP]  SecondsTillWakeup: "));
    Serial.println(secondsTillWakeup);
    Serial.print(F("[SLEEP]  Sleep duration millis: "));
    Serial.println(sleepDurationMillis);
#endif
    TimerSetValue(&wakeUpTimer, sleepDurationMillis);
    TimerStart(&wakeUpTimer);
  }
    
  powerDown();
}

void powerDown() {
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SLEEP]  Power down"));
#endif
  setprogramState(SLEEP_S);
  led::allOff();
  sht40.reset();
  mc3479.stop();
  microphone::disable(); // Also disables battery
  Wire.end();
  vext::off();
  vext::disable();
  lora::off();
  delay(500);
}

void onWakeUp() {
  timeWakeup = millis();
  //timeWakeupAdjusted = 0;
  gPacketNum += 1;
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SLEEP]  Wake up"));
#endif
  vext::enable();
  vext::on();
  Wire.begin();
  mc3479.wake();
  lora::on();
#if defined(DEBUG) && DEBUG > 0
  led::greenOn();
#endif
  setprogramState(WAKEUP_S);
}

void onTxDone() {
#if defined(DEBUG) && DEBUG > 0
#endif
  switch (lastSentMessageCode) {
    case HEARTBEAT_CODE:
      //led::blueBlink();
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[LORA]   Heartbeat sent"));
#endif
      lora::receive(HEARTBEAT_INTERVAL_MS);
      break;
    case REQ_PAIR_CODE:
      pairAttempt += 1;
      //led::blueBlink();
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[LORA]   Pair sent"));
#endif
      lora::receive(lora::RX_TIMEOUT_MS);
      break;
    case REQ_READING_CODE:
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[LORA]   Reading sent"));
      //led::blueBlink();
#endif
      lora::receive(lora::RX_TIMEOUT_MS + (MAX_SENSORS - sensorCycleNumber) * SENSOR_SEND_INTERVAL_MS);
      break;
    default:
      break;
  }
}

void onTxTimeout() {
  lora::standby();
#if defined(DEBUG) && DEBUG > 0
#endif
  switch (lastSentMessageCode) {
    case HEARTBEAT_CODE:
      break;
    case REQ_PAIR_CODE:
      if (pairAttempt >= MAX_PAIR_ATTEMPTS) {
        startSleep();
      } else {
        pairAttempt += 1;
        sendPairInit(gBattery);
      }
      break;
    case REQ_READING_CODE:
      startSleep(0, 0, true);
      break;
    defaul:
      break;
  }
}

void onRxDone(uint8_t *payload, uint16_t size, int16_t rssi) {
  lora::state = LORA_RECEIVING_S;
#if defined(DEBUG) && DEBUG > 0
  Serial.print("[LORA]   length: ");
  Serial.println(size);
  Serial.print("[LORA]   rssi: ");
  Serial.println(rssi);
#endif
  uint8_t packetCode = payload[0];
  uint8_t packetMac[6]; // Recepient sensor unless packet is a reading response. Gateway mac in that case.
  packetMac[0] = payload[1];
  packetMac[1] = payload[2];
  packetMac[2] = payload[3];
  packetMac[3] = payload[4];
  packetMac[4] = payload[5];
  packetMac[5] = payload[6];
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[SYSTEM] PACKET MAC:  "));
  for (int i = 5; i >= 0; i--) {
    uint8_t byte = packetMac[i];
    if (byte < 16) {
      Serial.print(0);
    }
    Serial.print(byte, HEX);
  }
  Serial.println();
  Serial.print(F("[SYSTEM] MAC:  "));
  for (int i = 5; i >= 0; i--) {
    uint8_t byte = gMacBytes[i];
    if (byte < 16) {
      Serial.print(0);
    }
    Serial.print(byte, HEX);
  }
  Serial.println();
  Serial.print(F("[SYSTEM] GATEWAY MAC:  "));
  for (int i = 5; i >= 0; i--) {
    uint8_t byte = gGatewayMacBytes[i];
    if (byte < 16) {
      Serial.print(0);
    }
    Serial.print(byte, HEX);
  }
  Serial.println();
#endif
  if (compareMacs(packetMac, gMacBytes)) {
    switch ((MessageCode)packetCode) {
      case RESP_PAIR_CODE:
      {
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[LORA]   Pair response received"));
#endif
        pairAttempt = 0;
        handlePairResponse(payload);
        gBattery = battery::readPercentage();
        sendHeartbeat(gBattery);
        break;
      }
      default:
        break;
    }
  } else if (compareMacs(packetMac, gGatewayMacBytes)) {
    switch ((MessageCode) packetCode) {
      case START_CYCLE_CODE:
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[LORA]   Start cycle received"));
#endif
        pairAttempt = 0;
        handleStartCycleRequest(payload);
        break;
      case RESP_READING_CODE:
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[LORA]   Reading response received"));
#endif
        handleReadingResponse(payload);
        break;
      default:
        break;
    }    
  }
}

void onRxTimeout() {
  lora::standby();
#if defined(DEBUG) && DEBUG > 0
#endif
  switch (lastSentMessageCode) {
    case HEARTBEAT_CODE:
    {
      gBattery = battery::readPercentage();
      sendHeartbeat(gBattery);
      break;
    }
    case REQ_PAIR_CODE:
      if (pairAttempt >= MAX_PAIR_ATTEMPTS) {
        startSleep();
      } else {
        sendPairInit(gBattery);
      }
      break;
    case REQ_READING_CODE:
      startSleep(0, 0, true);
      break;
  }
}

void setup() {
  vext::enable();
  vext::on();

  Wire.begin();
#if defined(DEBUG) && DEBUG > 0
  Serial.begin(115200);
#endif

  // Doesn't return on first init
  initialize();

  led::greenOn();

  battery::enable();

  memcpy(gMacBytes, &gMacU64, sizeof(gMacBytes));
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[SYSTEM] MAC:  "));
  for (int i = 5; i >= 0; i--) {
    uint8_t byte = gMacBytes[i];
    if (byte < 16) {
      Serial.print(0);
    }
    Serial.print(byte, HEX);
  }
  Serial.println();
#endif

  TimerInit(&wakeUpTimer, onWakeUp);

  if (!sht40.begin()) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[SYSTEM] Failed to initialize SHT40"));
#endif
  }

  if (!mc3479.start(1)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[SYSTEM] Failed to initialize MC3479"));
#endif
  }

  lora::on();
  setprogramState(LORA_S);
  gBattery = battery::readPercentage();
  sendPairInit(gBattery);
}

void loop() {
  switch (programState) {
    case LORA_S:
      switch (lora::state) {
        case LORA_STANDBY_S:
          break;
        case LORA_START_SEND_S:
          break;
        case LORA_SENDING_S:
          if (millis() - sendStartMillis > lora::TX_TIMEOUT_MS) {
            onTxTimeout();
          }
          break;
        case LORA_SENT_S:
          onTxDone();
          break;
        case LORA_START_RECEIVE_S:
          break;
        case LORA_RECEIVING_S:
          // Sometimes the millis() function jumps as seen in this example serial output:
          // [21:19:59.412161] [SLEEPTEST]  Receive end: 26077091
          // [21:19:59.538173] [SLEEPTEST]  Rx timeout: 157149220
          // 
          // timeWakeup is used to check if the millis have jumped.
          //
          // If the duration between timeWakeup and now is bigger than 1 minute
          // we assume the millis have jumped and we adjust receiveStartMillis and timeWakeup
          if (currentReceiveTimeout != 0) {
            uint32_t now = millis();
            uint32_t durationSinceWakeup = now > timeWakeup ? now - timeWakeup : 0xFFFFFFFF - timeWakeup+ now;
            if (durationSinceWakeup > 60 * S_TO_MS) {
              bool jumpedForward = now > timeWakeup ? 1 : 0;
#if defined(DEBUG) && DEBUG > 0
              if (jumpedForward) {
                Serial.print("[SLEEPTEST] MILLIS JUMPED FORWARD: ");
                Serial.println(millis());
              } else {
                Serial.print("[SLEEPTEST] MILLIS JUMPED BACKWARDS OR OVERFLOW: ");
                Serial.println(millis());
#endif
              }
#if defined(DEBUG) && DEBUG > 0
              Serial.println("[SLEEPTEST] BEFORE UPDATE: ");
              Serial.print("[SLEEPTEST] receiveStartMillis");
              Serial.println(receiveStartMillis);
              Serial.print("[SLEEPTEST] timeWakeup: ");
              Serial.println(timeWakeup);
#endif
              receiveStartMillis += durationSinceWakeup - sinceWakeupBeforeJump;
              timeWakeup += durationSinceWakeup;
              sinceWakeupBeforeJump = timeWakeup + sinceWakeupBeforeJump;
#if defined(DEBUG) && DEBUG > 0
              Serial.println("[SLEEPTEST] AFTER UPDATE: ");
              Serial.print("[SLEEPTEST] receiveStartMillis: ");
              Serial.println(receiveStartMillis);
              Serial.print("[SLEEPTEST] timeWakeup: ");
              Serial.println(timeWakeup);
#endif
            } else {
              sinceWakeupBeforeJump = now - timeWakeup;
            }
            
            if (now - receiveStartMillis > currentReceiveTimeout) {
#if defined(DEBUG) && DEBUG > 0
              Serial.print("[SLEEPTEST] TIMEOUT: ");
              Serial.println(millis());
              Serial.print("[SLEEPTEST] now: ");
              Serial.println(now);
              Serial.print("[SLEEPTEST] receiveStartMillis: ");
              Serial.println(receiveStartMillis);
              Serial.print("[SLEEPTEST] currentReceiveTimeout: ");
              Serial.println(currentReceiveTimeout);
              Serial.print("[SLEEPTEST] now/start diff: ");
              Serial.println(now - receiveStartMillis);
#endif
              onRxTimeout();
            }
          }
          break;
        case LORA_RECEIVED_S:
          {
            uint16_t length = lora::radio.getPacketLength();
            uint8_t data[length];
            lora::radio.readData(data, length);
            int16_t rssi = lora::radio.getRSSI();
            onRxDone(data, length, rssi);
            break;
          }
      }
      break;
    case WAKEUP_S:
      performReadings();
      setprogramState(LORA_S);
      sendReading(&reading);
      break;
    case SLEEP_S:
      lowPowerHandler();
      break;
  }  
}
