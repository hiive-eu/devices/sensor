#include "LoRa.h"
#include "Defines.h"

namespace lora {
  volatile LoRaState state;
  SX1262 radio = new Module(RADIOLIB_BUILTIN_MODULE);

  void internalOnTxDone() {
    state = LORA_SENT_S;
  }
  
  void internalOnRxDone() {
    state = LORA_RECEIVED_S;
  }
  
  int16_t on() {
    state = LORA_STANDBY_S;
    return radio.begin(LORA_FREQUENCY, LORA_BANDWITH, LORA_SPREADING_FACTOR, LORA_CODING_RATE,
                LORA_SYNC_WORD, LORA_TRANSMISSION_POWER, LORA_PREAMBLE_LENGTH);  
  }
  int16_t off() {
    return radio.sleep();
  }
  int16_t standby() {
    return radio.standby();
  }
  int16_t send(uint8_t* data, uint8_t length) {
    radio.setPacketSentAction(internalOnTxDone);
    sendStartMillis = millis();
    state = LORA_SENDING_S;
    return radio.startTransmit(data, length);
  }
  int16_t receive(uint16_t timeout) {
    uint32_t now = millis();
    radio.setPacketReceivedAction(internalOnRxDone);
    currentReceiveTimeout = timeout;
    receiveStartMillis = now;
    state = LORA_RECEIVING_S;
    int16_t res = radio.startReceive();
    return res;
  }
}
