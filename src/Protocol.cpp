#include "Protocol.h"
#include "LoRa.h"
#include "Microphone.h"

void sendHeartbeat(uint8_t battery) {
  uint8_t txBuffer[HEARTBEAT_PAYLOAD_SIZE];
  txBuffer[0] = HEARTBEAT_CODE;
  txBuffer[1] = gMacBytes[0];
  txBuffer[2] = gMacBytes[1];
  txBuffer[3] = gMacBytes[2];
  txBuffer[4] = gMacBytes[3];
  txBuffer[5] = gMacBytes[4];
  txBuffer[6] = gMacBytes[5];
  txBuffer[7] = battery;

  setLastSentMessage(HEARTBEAT_CODE);
  lora::send(txBuffer, HEARTBEAT_PAYLOAD_SIZE);
}

void sendPairInit(uint8_t battery) {
  uint8_t txBuffer[REQ_PAIR_PAYLOAD_SIZE];
  txBuffer[0] = REQ_PAIR_CODE;
  txBuffer[1] = gMacBytes[0];
  txBuffer[2] = gMacBytes[1];
  txBuffer[3] = gMacBytes[2];
  txBuffer[4] = gMacBytes[3];
  txBuffer[5] = gMacBytes[4];
  txBuffer[6] = gMacBytes[5];
  txBuffer[7] = battery;
  
  setLastSentMessage(REQ_PAIR_CODE);
  lora::send(txBuffer, REQ_PAIR_PAYLOAD_SIZE);
}

void sendReading(Reading *reading) {
  uint8_t txBuffer[REQ_READING_PAYLOAD_SIZE];
  txBuffer[0] = REQ_READING_CODE; 
  txBuffer[1] = gMacBytes[0]; 
  txBuffer[2] = gMacBytes[1]; 
  txBuffer[3] = gMacBytes[2]; 
  txBuffer[4] = gMacBytes[3]; 
  txBuffer[5] = gMacBytes[4]; 
  txBuffer[6] = gMacBytes[5]; 
  txBuffer[7] = reading->battery >> 8;
  txBuffer[8] = reading->battery & 0xFF;
  txBuffer[9] = reading->temperature;
  txBuffer[10] = reading->humidity;
  txBuffer[11] = reading->accX >> 8;
  txBuffer[12] = reading->accX & 0xFF;
  txBuffer[13] = reading->accY >> 8;
  txBuffer[14] = reading->accY & 0xFF;
  txBuffer[15] = reading->accZ >> 8;
  txBuffer[16] = reading->accZ & 0xFF;
  for (int i=0; i<microphone::NUM_OF_FREQUENCY_BINS; i++) {
    uint8_t soundPressure = round(reading->frequencyBins[i]);
    txBuffer[17+i] = soundPressure;
  }
  txBuffer[37] = (uint8_t) round(reading->soundLevel);
  txBuffer[38] = reading->packetNum >> 8;
  txBuffer[39] = reading->packetNum & 0xFF;

  setLastSentMessage(REQ_READING_CODE);
  lora::send(txBuffer, REQ_READING_PAYLOAD_SIZE);
}
