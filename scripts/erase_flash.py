import os
import time
import subprocess

Import("env")

def is_nixos():
    try:
        with open('/etc/os-release') as f:
            os_release = f.read()
        if 'ID=nixos' in os_release:
            return True
    except FileNotFoundError:
        pass
    
    return False

print("PRESCRIPT: erase_flash.py")
    
UPLOAD_PORT = env.get("UPLOAD_PORT")
if UPLOAD_PORT != None:
    HOME = os.getenv("HOME")
    PWD = os.getenv("PWD")
    command = f"{HOME}/.platformio/packages/tool-cubecellflash/CubeCellflash -serial {UPLOAD_PORT} scripts/erase_flash.cyacd"

    if is_nixos():
        command = "steam-run " + command

    p = subprocess.call(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    time.sleep(2)
    
    print("Flash erased")
