#!/usr/bin/env bash
if [ $# -eq 0 ]
then
  echo "Must supply upload port"
  exit 1
fi
pio run -e prod --target upload --upload-port="$1" && picocom -b 115200 "$1" --imap lfcrlf | ts "[%H:%M:%.S]"

