#ifndef READING_H
#define READING_H

#include "Microphone.h"
#include <Arduino.h>

struct Reading {
  uint8_t battery;
  int8_t temperature;
  uint8_t humidity;  
  uint16_t accX;
  uint16_t accY;
  uint16_t accZ;
  uint16_t amplitude;
  uint16_t packetNum;
  float frequencyBins[microphone::NUM_OF_FREQUENCY_BINS] = {0.0};
  uint8_t soundLevel;
};
#endif
