#ifndef DEFINES_H
#define DEFINES_H

enum LoRaState {
  LORA_STANDBY_S,
  LORA_START_SEND_S,
  LORA_SENDING_S,
  LORA_SENT_S,
  LORA_START_RECEIVE_S,
  LORA_RECEIVING_S,
  LORA_RECEIVED_S
};
  
#endif
