#ifndef BATTERY_H
#define BATTERY_H

#include <Arduino.h>
#include "Hardware.h"

namespace battery {
  constexpr uint32_t VBAT_R55 = 100000;
  constexpr uint32_t VBAT_R54 = 220000;
  constexpr uint16_t VBAT_MIN = 3500; // SHOULD BE 3500 according to Reger
  constexpr uint16_t VBAT_MAX = 4200; // SHOULD BE 4200 according to Reger
  
  constexpr uint8_t BATTERY_WARMUP_SAMPLES = 20;
  constexpr uint8_t BATTERY_SAMPLES = 10;
  
  void enable();
  void disable();
  uint16_t readVoltage();
  uint8_t readPercentage();
  
  uint8_t batteryVoltageToPercentage(uint16_t voltage);
}


#endif
