#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <Arduino.h>

#include "LoRa.h"
#include "Reading.h"

// PACKET SIZES
constexpr uint8_t HEARTBEAT_PAYLOAD_SIZE          =  8; // 1: packetCode
                                                        // 6: sensorMac
                                                        // 1: battery

constexpr uint8_t START_CYCLE_PAYLOAD_SIZE        = 10; // 1: packetCode
                                                        // 6: sensorMac
                                                        // 1: sleepInterval
                                                        // 1: minutesTillWakeup
                                                        // 1: secondsTillWakeup

constexpr uint8_t REQ_PAIR_PAYLOAD_SIZE           =  8; // 1: packet code
                                                        // 6: sensorMac
                                                        // 1: battery

constexpr uint8_t RESP_PAIR_PAYLOAD_SIZE          = 14; // 1: packetCode
                                                        // 6: sensorMac
                                                        // 1: sensorNum 
                                                        // 6: gatewayMac

constexpr uint8_t REQ_READING_PAYLOAD_SIZE        = 40; //  1: packet code
                                                        //  6: sensorMac
                                                        //  2: battery
                                                        //  1: temperature
                                                        //  1: humidity
                                                        //  2: imu_x
                                                        //  2: imu_y
                                                        //  2: imu_z
                                                        // 20: frequencyBins
                                                        //  1: soundLevel
                                                        //  2: packetNum

constexpr uint8_t RESP_READING_PAYLOAD_SIZE       = 10; // 1: packetCode
                                                        // 6: gatewayMac
                                                        // 1: sleepInterval
                                                        // 1: minutesTillWakeup
                                                        // 1: secondsTillWakeup

enum MessageCode {
  // Sensor requests w.o. response
  HEARTBEAT_CODE = 0x01,

  // Gateway requests w.o. response
  START_CYCLE_CODE = 0x02,

  // Sensor requests with gateway responses
  REQ_PAIR_CODE = 0x10,
  RESP_PAIR_CODE = 0x20,

  REQ_READING_CODE = 0x11,
  RESP_READING_CODE = 0x21,
};

extern void setLastSentMessage(MessageCode lastMessageCode);

extern uint8_t gMacBytes[6];

void sendHeartbeat(uint8_t battery);

void sendPairInit(uint8_t battery);
void sendReading(Reading *reading);

#endif
