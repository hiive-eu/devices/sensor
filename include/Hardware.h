#ifndef HARDWARE_H
#define HARDWARE_H

#include <Arduino.h>

constexpr uint8_t PIN_DOUT_VEXT = GPIO6;

constexpr uint8_t PIN_ADC_BATTERY = ADC;

constexpr uint8_t PIN_DOUT_LED_R = GPIO5; // on / off
constexpr uint8_t PIN_PWM_LED_G = PWM2;  // PWM 0x0 - 0xffff TODO
constexpr uint8_t PIN_PWM_LED_B = PWM1;  // PWM 0x0 -  0xffff TODO

constexpr uint8_t PIN_ADC_MICROPHONE = ADC;

constexpr uint8_t PIN_ADC_ANALOGSWITCH = ADC;
constexpr uint8_t PIN_DOUT_TOGGLE_ANALOGSWITCH = GPIO1;

constexpr uint8_t PIN_MP2667_ISR_SHIPPING = GPIO7;
constexpr uint8_t I2C_ADDR_MP2667 = 0x09;

namespace vext {
  void enable();
  void disable();
  void on();
  void off();
}

#endif
