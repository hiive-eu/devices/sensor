#ifndef STORAGE_H
#define STORAGE_H

#include <Arduino.h>
#include <EEPROM.h>

namespace storage {
  constexpr uint8_t STORAGE_INIT_ADDR = 0x00;
  constexpr uint8_t STORAGE_SHIPPING_DISABLED_ADDR = 0x01;
  
  bool readInitialized();
  bool readShippingDisabled();
  
  void writeInitialized(bool initialized);
  void writeShippingDisabled(bool shippingDisabled);
}

#endif
