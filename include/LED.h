#ifndef LED_H
#define LED_H

#include <Arduino.h>
#include "Hardware.h"

namespace led {
  void redOn();
  void redOff();
  void greenOn();
  void greenOff();
  void blueOn();
  void blueOff();
  void cyanOn();
  void magentaOn();
  void yellowOn();
  void blueBlink();
  void allOff();
}

#endif
