#ifndef MISC_H
#define MISC_H

#include <Arduino.h>

extern uint8_t chipIDBytes[6];

void copyMacBytesToAscii(char macAscii[13], uint8_t macBytes[6]);
bool compareMacs(uint8_t mac1[6], uint8_t mac2[6]);

#endif
