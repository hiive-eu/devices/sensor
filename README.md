# *** NOT PRODUCTION READY ***

Known shortcomings:

* Make sure that licenses of used packages are not violated
* Device needs to be certified

Unknown shortcomings: most likely.

# Configuration and building

The firmware is built using PlatformIO and comes in two different variations. `dev` and `prod`.

To flash the gateway run the following command where `<ENV>` is replaced with either `dev` or `prod`:

```
pio run -e <ENV> --target upload --upload-port=/dev/ttyUSB0
```

Connect to serial port using `picocom`:

```
picocom -b 115200 /dev/ttyUSB0 --imap lfcrlf | ts "[%H:%M:%.S]"
```

There are helper scripts in the `scripts` directory.


Parameters for configuring LoRa is set in `platformio.ini`

# Radio power configurations

The transmission power of LoRa is configured very conservatively, to ensure that the devices comply to the regulations related to
transmission power of devices.

# Shipping mode

The sensor will go into shipping mode (battery cut-off) on its first run. Shipping mode is disable by connecting the sensor over USB.

# Colors

Only supports 5 hardcoded colors.
It is difficult to make more distinguishable colors, because the red LED can only be off or full on.
Another way to disinguish more sensors using colors, could be to applly a blinking pattern to some of them.

Would be better if the gateway determined the color of the sensors instead of hardcoding them. But time...
